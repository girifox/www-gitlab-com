---
layout: markdown_page
title: "Greenhouse"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## For all Greenhouse users

### How to join Greenhouse

[Greenhouse](www.greenhouse.io) is GitLab's ATS (Applicant Tracking System). All hiring managers and interviewers will use Greenhouse to review resumes, provide feedback, communicate with candidates, and more.

You will receive an email to log in to Greenhouse for the first time. When you [log in to Greenhouse](https://app2.greenhouse.io/users/sign_in), choose "Sign in with Google," making sure you're already logged into your GitLab email account. All GitLab team members are granted basic access, which will allow them to add and track referrals and share job postings on social media. Until their access is upgraded by an admin, they will only have access to one page in Greenhouse, [Dashboard](https://app2.greenhouse.io/dashboard), where employees can make referrals. If you need your access to be upgraded so you can participate in the hiring process for a vacancy, please reach out to Recruiting.

### Making a referral

All GitLab team members can [make a referral](https://support.greenhouse.io/hc/en-us/articles/201982560-Submit-Referrals-from-the-Dashboard) directly on the [Greenhouse dashboard](https://app2.greenhouse.io/dashboard) by pressing the "Add a referral" button. You do not need to select an "Office" as this will limit the jobs you're able to select in the list. Please complete the required fields, as well as **being sure to include their email address**. We request you either upload their resume using the upload button at the top left or include a link to their LinkedIn profile, or we will reach out to you to get this information.

If you would like to [share a job posting](https://support.greenhouse.io/hc/en-us/articles/200721674-Generate-My-Referrer-Link) with a particular person or on social media, you can do it via the "Share your link with people interested in working at your company" option in Greenhouse below the "Add a referral" button. Simpily choose the role you wish to share, and copy and paste the link. You can also share jobs directly with your social media by connecting your Twitter, Facebook, or LinkedIn accounts to Greenhouse (also located on your dashboard). Once you connect your account, you can [schedule posts](https://support.greenhouse.io/hc/en-us/articles/200925999-How-do-I-schedule-posts-to-my-social-networks-) for the future and monitor them through your dashboard.

## For all interviewers

### Feedback and interview kits

When you have an interview, you have two ways to get to your interview kit to leave feedback for a candidate. The first is by clicking the link in the calendar invitation for the interview. The second is by going to your Greenhouse [dashboard](https://app2.greenhouse.io/dashboard) where you will see a list of your upcoming interviews and can click "See Interview Kit".

The interview kit consists of a few tabs:
- Interview Prep
  - Quick overview of what you'll want to look for in the interview, as well as any notes that previous interviewers had that they want you to look into further.
- Job Details
  - The full vacancy description posted on Greenhouse.
- Resume
  - This tab will only show up if the candidate provided a resume.
- LinkedIn
  - This tab will only show up if the candidate provided their LinkedIn profile.
- Scorecard
  - This is where you will enter all of your notes, score various attributes, and make a decision. (More detail below!)

Additionally, the interview kit shows the candidate's name, contact details, pop-out links to their resume, cover letter, and other details, as well as the details for the interview (location, time, etc.) on the lefthand side. It also includes the role the candidate is interviewing for at the top of the page.

#### Scorecards

The scorecard consists of a few elements. There are text boxes where you can add in your notes. Some scorecards for different roles or stages may have additional text boxes for specific questions. If a text box is required, there will be a red asterisk by the question.

Underneath the first text box "Key Take-Aways", there are two additional links, `Private Note` and `Note for Other Interviewers`, which will open additional text boxes if you click on them. Private notes are typically only used by the recruiting team when collecting compensation information and are only viewable by the recruiting team and hiring managers for the role. The **note for other interviewers** text box is extremely useful to all interviewers, as this is a place where you can include any information that you think would be relevant in future interviewers, including areas to dig into, further evaluate, or look out for. Any notes you put in this text box will appear in the next interviewers' interview kit on the "Interview Prep" tab.

Below the text boxes for notes, each role has a list of attributes that we are looking for. Each stage in the process has certain attributes highlighted that it's recommended for you to evaluate; however, no attributes are required, so you can feel free to rate any attributes you've gained insight into. All stages include attributes for values-alignment, which all interviewers are heavily encouraged to complete, so we can assess values-alignment for each candidate.

Below the attributes is the final piece where you will make your decision. Greenhouse says "Overall Recommendation: Did the candidate pass the interview?"; this should be interpreted as "Do you want to hire this candidate?" and answered appropriately. The final score is not required by Greenhouse, but it must be completed by the interviewer. If the interviewer does not add their vote, the recruiting team will follow up with them to include their vote. If you are on the fence about a candidate, that is typically an indication that you do not want to hire the candidate, but if you are really unsure feel free to reach out to your recruiter to discuss further. Your recruiter may agree with your hesitations and decline, or you may agree that there is an element that should be further explored in an additional interview.

Your scorecard will automatically save as you enter information, and record that it saved it the top righthand corner. If there is an error and Greenhouse is not able to save the scorecard, it will say so at the top righthand corner and you'll be unable to submit your scorecard. You can either wait until it does save, or open the interview kit again and copy/paste the information over. If you have further issues, please reach out to the recruiting team.

Finally, click `Submit Scorecard` and you're done! From there, you may go back to review and edit your scorecard, view the candidate profile, or return to your dashboard.

#### Interview notifications

You can set up reminders in Greenhouse by going to your [account settings](https://app2.greenhouse.io/myinfo) and turning on `Daily interview reminder email` which will email you each morning with a list of your interviews for the day. You can also connect your Slack account to your Greenhouse account and receive reminders after the interview is over if your scorecard is still due.

## For Hiring Managers

### What access do you have?

As a hiring manager or executive, you have access to view all candidates for any vacancies you are marked as a `Hiring Manager` for, as well as all private notes for any of those candidates. While interviewers are restricted in that they are not able to see previous notes or feedback for a candidate when they are scheduled for an interview, hiring managers do not have that restriction and are able to see all notes and feedback at any point in the process. Hiring managers are not able to view candidates anymore once they are hired, or any candidates outside of the role they are a hiring manager for.

## For Recruiting

### How to upgrade access levels

Only Admins can upgrade another team member's access level. The default is "Basic", but anyone reviewing and interviewing candidate profiles should be upgraded to the "Interviewer" role. To do this:
 1. Go to [settings](https://app2.greenhouse.io/configure) and click on [users](https://app2.greenhouse.io/account/users?status=active)
 1. Search for the team member in the search bar; if their name appears, click on their name
 1. Click "Edit" in the permissions section. Choose the "Job Admin/Interviewer" option
 1. Click "Add" under "Job-Based Permissions"
 1. Search for the job they should have access to and click "add" next to it, then choose "Interviewer"

For any team members who are hiring managers (or above), choose "Job Admin: Hiring Manager" for the specific roles they should have access to; if they are a hiring manager for an entire department, they should also be added as "Job Admin: Hiring Manager" for future roles within that department. If they are a hiring manager or above for a role, they should always be marked as "Job Admin: Hiring Manager" and _not_ "Interviewer" even if they will be interviewing, since that will limit their access. Meanwhile, the executive for a division (e.g. CRO, CMO, VPE, etc.) should receive "Job Admin: Job Approval" for all of their division's current and future roles in order to give them the same permissions as "Job Admin: Hiring Manager" as well as the ability to approve new vacancies.

Similarly, Recruiting receives "Job Admin: Recruiting" for all current and future roles; People Ops receives "Job Admin: People Ops" for all current and future roles.

### Changing jobs

If a candidate applies for a job but is a better fit for another job, they will need to be added to a new job. There are a few options to do this; to start, go to the candidate profile and click "Add or Transfer Candidate's Jobs" at the bottom right. Fro there, you can either add a new job to the candidate profile, which will keep all data within the original job and start a clean slate for the new job. Alternatively, you can also transfer the candidate data to the new job, but this should be done with caution, as it will not transfer any scheduled (i.e. not completed and submitted) interviews or scorecards, and it will remove the candidate from any reports about the original job. Super Admins are also able to remove a job and its history from a candidate profile, which should be done only when absolutely necessary.

### Scheduling interviews with Greenhouse

The recruiting team will schedule most interviews, which is a three-step process.

To schedule an interview, first make sure to move the candidate to the correct stage (Screening, Team Interview, Executive Interview, etc.), then request the candidate's availabilty by pressing the "request availibility" button on their profile. You will recieve an email once they send over their availability. It is recommended that you modify the email with the general work hours of the interviewer for smoother scheduling.

Once the candidate confirms their availability, you can schedule by clicking the "Schedule Interview" button on the candidate's profile. Go to the "Find Times" section, and find a free spot on the interviewer's calendar. Note both the candidate's and interviewer's time zone for easier scheduling. To change the calendar's time zone, you can click settings and click the drop-down with your time zone. Greenhouse will show the available time of the interviewee in white on the calendar. If you do not find a suitable time on the first day, you can change days by clicking the arrows near the current day. Note that Greenhouse **does not** currently update time zones once you change pages. You will need to reselect the time zone again even if it looks like it is selected in the timezone bar.

Once a time is set, you'll need to send the candidate an interview confirmation. You can do so by clicking the "Send Interview Confirmation" button. This will bring up an email template. Note that the time zone defaults to your time zone even on the candidate's side. Therefore, it is recommended to do the conversion for the candidate to avoid confusion or highlight that it is in your time zone. There are [timezone tools](https://www.worldtimebuddy.com/) you can use for timezone conversions. Always select the send calendar invitation on the bottom of the email and then send the email.

The recruiting team will schedule all interviews and send an calendar invite with a link to a Zoom room for *all* interviewers, which will follow this format: `https://gitlab.zoom.us/my/gitlab.firstnamelastname`  (e.g. Joe Smith's would be: `https://gitlab.zoom.us/my/gitlab.joesmith`). This room will be your consistent location for your interviews and the naming convention is standard across GitLab. All interviews will be conducted via Zoom to create a streamlined hiring process. To create a personal Zoom Room for someone, please follow the [instructions in the handbook](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/#make-zoom-pro).

#### Important scheduling notes

* Please be sure to go to your [Google Calendar settings](https://calendar.google.com/calendar/r/settings?tab=mc) and input your current time zone.
* Make sure that a candidate is in the proper stage to schedule when scheduling.
* If you cannot attend the interview, please let the recruiting team know so they can reschedule. **Simpily declining the interview does not effectively notify the scheduler.** The recruiting team should first cancel the currently scheduled interview through either Greenhouse or Google Calendar (specifically the `Interview Calendar`) before creating the new one to avoid any confusion.
* All emails and scheduling must be done through Greenhouse.
* Emails and communication between team members can be viewed in the activity feed of a candidate.
* Timezones can be tricky with Greenhouse. Always double check timezones before sending confirmations, noting that all interview confirmations and the time shown on the candidate's profile is in your time zone.  
* You can find a candidate's timezone by clicking the activity feed.
* To ensure that someone else (another candidate, for example) does not join your video call without your permission, log into your Zoom account online. Click on "My Account" in the top-right corner, and click on "Meeting Settings" in the left sidebar. Scroll down to "In Meeting (Advanced)," and toggle on the option "Waiting Room."

### Rejecting candidates

To remove a candidate from the active pipeline, they need to be rejected in Greenhouse, which is typically done only by recruiting. Any time a candidate is rejected, People Ops will email them letting them know, as they will not automatically be notified if we reject them. When clicking the reject button on a candidate, Greenhouse will open a pop up where you can choose the appropriate rejection reason, as well as a rejection email template. Feel free to adjust the template per the [guidelines in the handbook](https://about.gitlab.com/handbook/hiring/interviewing/#rejecting-candidates). You can also select a time delay to send out the rejection email. Finally, you are also able to start a new prospect process for a candidate when rejecting them, in the event you want to reach out to them again in the future.

You can also reject candidates in bulk by going to the [candidates page](https://app2.greenhouse.io/people), filtering accordingly, clicking `Bulk Actions` at the top right, selecting the appropriate candidates or by clicking "Select All", clicking "Edit Selected", clicking "Reject" in the pop up, and following the same procedure as above to reject and email. Note: you can only choose one reason when archiving in bulk.

To unarchive a candidate, click "Unreject" on the candidate profile under the appropriate role.

If you want to consider a candidate for the future, the best practice is to archive them with the reason "Future Interest"; however, you can also start a prospect process when rejecting them so they are including in a pool of future interest candidates for the role, or you can simply add a follow-up reminder for yourself, the recruiter, and/or the coordinator to reach out to the candidate in a certain amount of time.

### Changing stages for a candidate

Each role has similar stages, with various interviews within each stage. Below is a typical outline of the stages:

- Application Review
  - This stage typically consists only of one event, which is the application review, where all new applicants are added. Hiring managers and/or interviewers can leave a scorecard in this stage on if they want to advance or reject a candidate.
- Assessment (added per role)
  - This stage is only added to roles that use questionnaires as part of the interview process. The assessment is done completely within Greenhouse, where the questions are added per job, the candidate submits their answers via a link in Greenhouse, and the graders review the answers in Greenhouse and leave a scorecard.
- Screening
  - This stage has only the screening call, where the recruiting team does an initial call with the candidate to review their experience, skills, and values-alignment.
- Team Interview
  - The "Team Interview" usually has several interviews within it, typically with the hiring manager, two peers, a director, and/or a panel interview.
- Executive Interview
  - This stage consists of two interview events, one for the executive of the division and another for the CEO. Both of these interviews are optional.
- Reference Check
  - There are two spots for completed reference checks, one from a former peer and one from a former manager.
- Offer
  - This is where the recruiting team prepares the offer and the offer is approved, and is explored more in detail in the [interviewing section](https://about.gitlab.com/handbook/hiring/interviewing/#offer-package) of the handbook.

On rare occasion, there may be additional or less stages than represented here, but these stages should be consistent as much as possible in order to maintain data integrity for reporting. The interviews within the stages can be adjusted as needed, as long as they follow the same names (e.g. there should only be one `Peer Interview 1` across all jobs and not a `Peer Interview 1` on one job and a `Peer Interview One` on another). If there is any doubt or confusion, feel free to reach out to the Talent Operations Specialist.

If a candidate will have more interviews in a stage than predetermined, you can add additional interview events as long as the candidate is in the stage where you need to add the additional event.
