---
layout: markdown_page
title: "Content"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Content Marketing

The Content Marketing team includes audience development, editorial oversight,
social marketing, and content strategy, development, and
operations. The Content Marketing team is responsible for the stewardship of
GitLab's audiences, users, customers, and partners' content needs, preferences and
perceptions of GitLab. Content marketing creates engaging, inspiring, and
relevant content, executing integrated content programs to deliver useful and
cohesive content experiences that build trust and preference for GitLab.

## Other related pages

- [GitLab blog](/handbook/marketing/blog)
- [Editorial calendar](/handbook/marketing/corporate-marketing/content/editorial-calendar/)
- [Editorial review checklist](/handbook/marketing/corporate-marketing/content/editorial-review-checklist/)
- [Social media guidelines](/handbook/marketing/social-media-guidelines/)
- [Content Hack Day](/handbook/marketing/corporate-marketing/content/content-hack-day)
- [Newsletters](/handbook/marketing/corporate-marketing/content/newsletters)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Communication

### Chat
Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of new, published content
- `#blog` for questions regarding blog content
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

### Issue trackers
  - [General Content Marketing](https://gitlab.com/gitlab-com/marketing/general/boards/104871?=)
  - [Blog posts](https://gitlab.com/gitlab-com/blog-posts/boards/409030)

### Team
- **Erica Lindberg, Manager, Content Marketing**
  - Area of focus: Leadership, case studies
  - Contact: [@erica](https://gitlab.com/erica)
- **Rebecca Dodd, Managing Editor**
  - Area of focus: Blog endboss, editing blog posts
  - Contact: [@rebecca](https://gitlab.com/rebecca)
- **Emily von Hoffmann, Associate Social Marketing Manager**
  - Area of focus: Social media and Medium (50%), Net new content for GitLab blog (50%)
  - Contact: [@evhoffmann](https://gitlab.com/evhoffmann)
- **Aricka Flowers, Content Marketing Associate, Ops**
  - Area of focus: Writing net new content for GitLab blog (100%)
  - Contact: [@atflowers](https://gitlab.com/atflowers)
- **Suri Patel, Content Marketing Associate, Dev**
  - Area of focus: Writing new content for GitLab blog (100%)
  - Contact: [@spatel](https://gitlab.com/suripatel)

## Editorial Mission Statement

Empower and inspire software teams to adopt and evolve a DevOps workflow to collaborate better, be more productive, and ship faster by sharing insightful and actionable information, advice, and resources.

### Vision

Build the largest and most diverse community of cutting edge co-conspirators who are leading the way to define and create the next generation of software development practices.

### 2018 Core content strategy statement

The content we produce helps increase awareness of GitLab’s complete and single application with the goal of broadening our market share and increasing sales by providing informative and persuasive content that makes DevOps teams feel excited, curious, and confident so that they can adopt & integrate industry best practices into their workflow.

## Messaging for Verticals
When to develop vertical messaging: The key is to determine if an industry has a certain pain point that another industry does not share.  You need to describe the problem (using industry specific terminology, if necessary) and also how your product solves these problems for them. Additionally, you can create high-level messaging and then branch off; for example if multiple industries are very security conscious, create security focused marketing, and adapt to select high value verticals.

## Checklist for good content<a name="checklist"></a>

- Relevant
- Useful
- User or customer-centered
- Clear, Consistent, Concise
- Supported

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first. 
1. If you need help finding relevant content to use, ask for help in the #content channel. 
1. If the content you're looking for doesn't exist, open an issue in [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) and label it `blog post`. The content team will evaluate if it's likely do well on the blog, in which case we will write the content. If the suggestion isn't likely do well, we will suggest you write it and we will help edit it. 
1. If you are creating your own content and need a copy review, ping @erica. Please give at least 3 days notice. 

## Writing blog posts 

See the [blog handbook](/handbook/marketing/blog/) for detailed instructions on how to publish a blog post. 

The Content Marketing team is responsible for increasing sessions on the GitLab blog month over month. We use data-drive insights to decide what to write on using the [content marketing dashboard](https://datastudio.google.com/u/0/reporting/1NIxCW309H19eLqc4rz0S8WqcmqPMK4Qb/page/1M). In order to hit our goals, we aim to publish at least 3 blog posts that will garner 10,000+ sessions. 

### Trends

*From [2018 blog analysis](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/248) conducted in 2018-10

1. Average sessions per month on the blog: ~30,000 
1. Average sessions per post in published month: 3,792
1. Average sessions per content marketing post in published month: 3,500
1. 55% of posts get <1,000 sessions in a month 
1. 27.47% of posts hit our expected outcome of 1,000-4,999 sessions in published month
1. 28.57% of posts garner less than 499 sessions in published month
1. 9% of posts are "hits" (10K+ sessions in published month); "Hits" don't consistently perform well over time

**Breakdown by category of "hits":**

- Content: 37.5%
- Product: 31.35%
- Corporate: 25%

### Obeservations

1. There is not a strong correlation between # of sessions and topic
1. Strong performing content marketing posts focus on show and tell engineering stories
1. Posts really fall into the 5,000-9,999 session bracket (3.3%)
1. Content hack day posts tend to perform well (>1,000 sessoins in published month)

### Identifying and qualifying a high-performing blog post 

**Qualifying story ideas:** 

Look for the following patterns: 

1. Team implementing a new techonology, process, or coding language
1. Deep dive into how a popular feature is made
1. Chronicling a performance improvement
1. Covering a controversial decision that was made

**Who and how to interview:**

1. Contact the technical subject matter expert. This can be someone who created the issue, or managed the project.
1. Set up a 30 minute interview and dig into: 
  - What was the challenge? 
  - What was the solution? 
  - How did you go from point a to point b? Walk me through your thought process. 
  - How was the solution implemented and what is a realistic use case of the solution? 
  - What lessons were learned? 
  - How did this make the GitLab product / development community better? 
1. Optional: Contact the business subject matter expert. This could be a product manager or a product marketing manager.
1. Set up a 30 minute interview and dig into:
  - How does this solution help a user?
  - What business value does this solution bring? 
  - How does this solution relate to our product? 

**Attributes of a successful blog post:**

1. Deep dive into a hard technical challenge.
1. Puts the reader in the shoes of the person who faced the challenge; reader learns via compelling example. 
1. Intellectually satisfying; learning compontent.
1. Allows the reader to learn from someone else's mistake, and offers a problem/trials and triumphs/solution sotry arch. 
1. Taking a controversial or unpopular stance on a topic, back by hard evidence. 

**Examples of high-performing posts (20K+ sessions in published month):** 

1. [Meet the GitLab Web IDE](/2018/06/15/introducing-gitlab-s-integrated-development-environment/)
1. [How a fix in Go 1.9 sped up our Gitaly service by 30x](/2018/01/23/how-a-fix-in-go-19-sped-up-our-gitaly-service-by-30x/)
1. [Hey, data teams - We're working on a tool just for you](/2018/08/01/hey-data-teams-we-are-working-on-a-tool-just-for-you/index.html)
1. [Why we chose Vue.js](https://about.gitlab.com/2016/10/20/why-we-chose-vue/)
1. [How we do Vue: one year later](/2017/11/09/gitlab-vue-one-year-later/)

### Conducting a blog analysis 

Pull information on all blog posts for document how many sessions each post received in the month, and how many sessions they received of all time. Categorize them by type, bracket, total sessions in month, total sessions to date, category, theme, and topic. Eventually add first touch point revenue data.
Search Google Drive for `Blog Performance` to find the appriopriate sheet to work from. 

- Blog post links should be added as they are published and category, audience, theme, and topic should be filled out.
- The Managing Editor and Manager, Content Marketing should review last month on the 1st of the month to fill out session information and make observations
- Review 1st of each quarter to update total sessions

**Column explanations:**

- Type: helps identify the frequency of which certain types of information is shared on our current blog
- Bracket: helps quickly sort blog posts by performance level
- Category: indicates class of information
- Total sessions in month: how many sessions the post received in the month it was published
- Audience: indicates who we expect to reach
- Theme: indicates the structure of the post
- Topic: indicates the main subject covered

### Crediting blog posts

Add [a note](/handbook/product/technical-writing/markdown-guide/#note) at the end of a blog post reading "[Name] contributed to this story/post" if:

- You ghostwrote the post on behalf of someone else who is listed as the author
- You were heavily involved in editing/structuring/rewriting the post authored by someone else
- You wrote the post, but it is a company announcement listing GitLab as the author
- You wrote the post and are listed as the author, but wish to give credit to interviewees

## Key Responsibilities

At the highest level, Content Marketing is responsible for building awareness, trust, and preference for the GitLab brand with developers, engineers, and IT professionals.

1. Audience development
2. Editorial voice and style
3. Defining and executing the quarterly content strategy
4. Lead generation

## Campaigns<a name="campaigns"></a>
Content marketing is responsible for executing marketing campaigns for GitLab. We define a campaign as any programmed interaction with a user, customer, or prospect.  For each campaign, we will create a campaign brief that outlines the overall strategy, goals, and plans for the campaign.

## Campaign Brief Process
To create a campaign brief, first start with the "campaign brief template" (which can be found by searching on the google drive). Fill out all fields in the brief as completely as possible. Certain fields might not be applicable to a particular campaign. For example, an email nurture campaign leveraging text based emails won’t have a visual design component. This field can be left blank in that example.

Once the campaign brief is filled out, create an issue in the GitLab Marketing project and link to the campaign brief.

On the GitLab issue, make sure to:
* Tag all stakeholders
* Use the Marketing Campaign label
* Set the appropriate due date (the due date should be the campaign launch date)
* If there are specific deliverables, create a todo list in the issue description for each stakeholder along with a due date

## Requesting Event Promotion <a name="requesting event promotion"></a>

#### Open a separate issue for social
- Open a separate issue for social in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) using the `event-social` label only *once the event has been approved* and *a landing page has been created* (i.e., try to only open an issue once it is ready for action.)
- For an event to get promoted on social, there must be a dedicated social issue. Mentioning social in the meta issue is insufficient. 
- If the need is urgent (i.e., the event is happening *now*), Slack Emily vH. She reserves the right not to publish on social.
- Luke will create social images if time permits - *at least* one week before desired publication is required for unique social images to be produced. If Luke has already been engaged for booth art, he may be able to turn them around on a shorter timeline, but this is at his discretion. 


#### What to expect
- Major corporate events will be promoted via organic social on GitLab's social channels. Emily vH will manage this.
- Field events with landing pages on about.gitlab.com will be promoted via paid social, so that we do not saturate our audience, and so that we can use the more advanced targeting that paid social allows. The Digital Marketing Specialist (Starts Nov. 5 - interim LJ) will manage this.
  - Don't worry about the divide between paid and organic - we will determine the best fit for the event and coordinate among ourselves.
- Note: the below are general suggestions based on best practices; the social media manager reserves the right to dial up or down the number of posts based on the event type and existing schedule.
- Once images are finalized, Emily vH will schedule social within 2 days.
- Tweets will go out from the corporate account: 1 week or more before event;  1 day before event; on the day of, with venue pictures if possible (see below)
- The corporate account will also selectively retweet relevant tweets from GitLabbers attending or speaking at the event.
- LinkedIn and Facebook posts will go out: one week or more before the event; a few days before the event. 

#### While at the Event:
- Take pictures of booth and/or team members at booth and send them over Slack to Emily vH. Be sure to include any updates to the booth location (for instance, if the venue is confusing, you can say "near the north entrance", etc.)
- Emily vH will aim to post them ASAP. If you are Slacking her outside of normal hours (for instance, for an event in EMEA), please have someone at the booth tweet from their own account using the event hashtag, and Emily vH will retweet from the corporate account when she starts the next day as a follow-up tweet.

## Giveaways <a name="giveaways"></a>

We use giveaways to encourage and thank our community for participating in marketing events such as surveys, user-generate-content campaigns, social programs, and more.

### Giveaways Process <a name="giveawaysprocess"></a>

**Pre-giveaway**
1. Create an issue and tag Emily von Hoffmann (@evhoffmann) to determine the
rules of engagement and Emily Kyle (@emily) for prizes.
2. Create and publish an [Official Sweepstakes Rules page](#officialrules)

**Post-giveaway**
1. Winners must sign an Affidavit of Eligibility & Liability, Indemnity, and Publicity Release. Use the "Affidavit of Eligibility - Sweepstakes" template found on the google drive.
2. Announce the winners

### Social Support & Logistics for Giveaways

#### Creating the Campaign

- Set a launch date
- Ask for social image(s) with text (if organic posts only) explaining the offer/ask
- Set an initial deadline for submissions, so you can have multiple pushes at interval & ramp up energy  
- Finalize the delivery method: form vs. tweets vs. retweets, depending on the goals of the campaign
    - Pros of a form: Neat, uniform, easy for us to keep track of, no downsides of low engagement (i.e., responses not visible)
    - Pros of asking for submissions via Twitter: we could more easily RT cool responses, get more out of a hashtag, etc.
    - Pros of asking for RTs in exchange for swag: very little backend to do on social afterwards, except to announce the winners of swag
- Finalize the ask, making sure it's extremely clear what you want to happen (`Share your GitLab story!` `Tell us your favorite thing you made with GitLab` `tell us a time GitLab helped you out of a tight spot`)
    - Make sure the ask can be intuitively communicated via whichever delivery method you're using, i.e., the tweet doesn't need to explain everything if you're pointing to a form or blog post. If you're not pointing to anything, make sure the tweet plus possible image text must make sense by themselves. Use threads for more space!

#### Pre-launch

- Finalize the timeline for when the reminders/follow-ups will go out, add to social schedule and leave some space around them to RT/engage with responses
- Finalize copy for all pushes
- If swag is involved, create a google sheet with swag codes from Emily Kyle
- Finalize hashtag
- Ask community advocates to review all copy (tweets, form, blog post) and adjust according to their suggestions
- Make sure the community advocates are aware of the campaign timeline/day-of
- Designate a social point person to be "on duty" for the day-of and one person who can serve as backup
- Let the broader GitLab team know that the social campaign is upcoming and ask for their support

#### Day of giveaway
- If you have entries for the giveaway in a spreadsheet, use [random.org](https://www.random.org/) to generate a random number. Match the number to the corresponding row in your spreadsheet to identify the winner. **Never enter email addresses or personal information of participants into a third-party site or system we do not control.**
- Try to schedule first push or ask a team member to tweet the first announcement early (ex: around 4 am PT) to try to have some overlap with all our timezones
- If you're asking for RTs in exchange for swag, make sure there's a clearly communicated cut-off to indicate that the giveaway will not stretch into perpetuity. One day-long is probably the longest you want a giveaway to stretch, or you can limit to number of items.
- Plan to engage live with people
   - If your promise was to give away one hoodie per 25 RTs, do it promptly after that milestone is crossed. It adds to the excitement and will get more people involved
- Announce each giveaway and use handles whenever possible, tell them to check their DMs
- DM the swag codes or whatever the item is
- In your copy, directly address the person/people like you are chatting with them irl
- RT and use gifs with abandon but also judgment

#### After the Giveaway
- Thank everyone promptly, internal & external
- Write in the logistics issue of any snags that came up or anything that could've gone better
- Amend hb as necessary for next time

### How to Create an Official Sweepstakes Rules Page <a name="officialrules"></a>

1. Create a new folder in `/source/sweepstakes/` in the www-gitlab-com project. Name the folder the same as the giveaway `/source/sweepstakes/name-of-giveaway`
2. Add an index.html.md file to the `/name-of-giveaway/` folder
3. Add the content of [this template](https://gitlab.com/gitlab-com/marketing/general/blob/0252a95b6b3b5cd87d537dabf3d1675023f1d07d/sweepstakes-official-rules-template.md) to the `index.html.md` file.
4. Replace all bold text with relevant information.
5. Create merge request and publish.

## Social Takeover Notes

So you've been nominated for a short-term takeover of GitLab's social channels — here's a quick guide with everything you need to know.

**Format:**
* Create [tracked links](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) for every link back to about.gitlab.com (e.g., all blog posts, landing pages, etc.)
* Publish and schedule in Sprout, using the “shorten links” button
* Choose a tag for every post that goes out (there is a dropdown)
* Preview posts in Sprout to make sure spacing, image, etc. looks fine on desktop and mobile

**Timing:**
* **Check the schedule in Sprout.**
* Do not publish more than 1-2 times per day on Facebook & LinkedIn (studies show > 2 posts per day on brand pages on these platforms starts to feel spammy).
* Do not publish tweets within 30 minutes of each other. Retweets of other stuff within that time is fine.
* Especially if someone reacts to an article that you tweeted with a substantive comment, question, or conversation starter, that is a good one to retweet promptly, because it could spur additional engagement.
* If you’re debating between posting early and posting late, post early (i.e., 4 am Pacific). We have a sizable audience in Europe, so it’s best to resist posting after 4 pm PT (unless it’s a retweet, or a post that is newsworthy and published late)

**Content:**
* Newly published posts
* Retweet of substantive, high quality tweets, especially from team members
* Archive posts
   * If you feel the day has been light on content, feel free to check out [sample copy & assets here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).
* Can also fill some space in the afternoon with a job posting (“jobs of the week” are in the [company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) doc)
* Have fun with it! Be liberal with emojis.

**Engagement:**
* In addition to retweeting described above, please perform a search on `#gitlab` and `gitlab` a few times per day (can be within Sprout or twitter) and favorite positive mentions
* Any particularly funny or complimentary posts, you can comment on with a gif, :blush:, “Thanks! We’re so glad you like it, let us know how we can help!” etc.
* Be creative but refer back to these guidelines for the [GitLab voice](https://about.gitlab.com/handbook/marketing/social-media-guidelines/#gitlab-voice)

**Signing off**
* If you know there is going to be a long (1+ days) delay between when you need to sign off and when the next person takes over (i.e., for Summit travel), I recommend scheduling a tweet saying something like “The team is heading to beautiful Cape Town for our xth Summit! See you on the other side :airplane:” This helps prime people for the disruption of regular content.

## Social Channels and Audience Segmentation

You can find a list of evergreen content assets their primary channel for promotion [here](https://docs.google.com/spreadsheets/d/1_RdHsIflAdLjvZKzCzMCa-xBgwZYoZgGBJr0E-6Djcs/edit?usp=sharing).

### Twitter

**Content/Execution**
* Broad audience - all our content gets shared here, but tone should still appeal to devs.
* 5 or more tweets per day, plus retweets
  * Schedule between 12 am - 5 pm PT (A large part of our audience is in EMEA, and we find an increase in organic impressions and engagement when we schedule tweets for their morning and workday)
  * At least 30 mins apart
* Aim for variety & roughly even mix of main topics: Git, CI/CD, collaboration, DevOps, employer branding, product-specific, community articles & appreciation
* Check mentions at least once per day, pull out articles to share with the team and favorite positive mentions.
*  Keep a list of our most engaged followers; keep these in mind for opportunities outside of social--blogging, live streamed interview etc.

**Goals**
* 15 tweets per day
* 2-4 videos published natively per month
* 5 lead-generating content items from the backlog published per month
* Consider [takeovers.](http://www.telegraph.co.uk/news/2017/07/12/work-experience-boy-takes-southern-rails-twitter-account-things/) Would need to develop an instructions kit & think about how/who to pilot.

### Facebook

**Content/Execution**
- Developer/community- and thought-leadership focused
- 1-2 facebook posts per day
- At least 2 hours apart
- Live events are more company/culture focused
    - talks by or AMAs with People Ops, etc.

**Goals**
- Consider streaming a webcast on Facebook Live (a simple toggle switch in Zoom allows this) and compare the performance to YouTube live streaming
- When the speaker cameras are enabled in Zoom webinars, natively publish clips of webcasts for post-promotion.

### LinkedIn
Sometimes, posts that GitLabbers propose for our blog may be a better fit for native publishing on LinkedIn. This is not a negative, it's usually due to the content team's strategic priorities at the time. Rebecca and Emily vH will recommend that you publish on your own LinkedIn, and if you agree, Emily vH will help you finalize the post and socialize it internally for best results. 

**Content/Execution**
- IT buyer/manager focus
- 3-5 posts per week
  - Non-tutorial original blog posts
  - Press releases/product announcements
- Posts around particular features, releases, company culture, people ops new practices, problem/solution or personal growth structured posts; include links back to homepage

**Goals**
* Identify key LinkedIn groups to join/share content in
* Native posts on LinkedIn
   * Encourage targeted employees to do so; think of incentive/framing as professional development that we will help edit.
* Curate and share articles from other publishers on remote work/culture that we want to elevate

### Medium
GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLabbers may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication. Emily vH will approve you as a writer and help finalize the post before publishing. 

**Content/Execution**
* Brand and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

### Google+

**Content/Execution**
- Re-post older articles from the archive periodically that we want to continue giving a boost
- Aim for at least one post per week

## Ensuring your Post Will Have a Functional Card and Image

When you post a link on Facebook or Twitter, either you can see only a link, or a full interactive card, which displays information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG]. Twitter Cards were recently set up for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot** of the post's cover image, taken from the [Blog landing page][blog]. This screenshot can be taken locally when previewing the site at `localhost:4567/blog/`.

### Defining Social Media Sharing Information

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'
```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.

#### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named after the page's file name.

For the second post above, note that the tweet image is the blog post cover image itself, not the screenshot. Also, there's no `description` provided in the frontmatter, so our Twitter Cards and Facebook's post will present the _fall back description_, which is the same for all about.GitLab.com.

For the handbook, make sure to name it so that it's obvious to which handbook it refers. For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

#### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](../blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

#### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)


<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->

<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
